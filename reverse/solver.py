from z3 import *

solver = Solver()

targetFullSum = 108
targetEvenSum = 191
targetOddSum = 173


def c(n):
    return globals()['b%i' % n]


for i in range(0, 14):
    globals()['b%i' % i] = BitVec('b%i' % i, 8)
    solver.add(
        Or(
            And(
                UGE(c(i), ord('A')),
                ULE(c(i), ord('Z'))
            ),
            And(
                UGE(c(i), ord('a')),
                ULE(c(i), ord('z'))
            ),
            And(
                UGE(c(i), ord('0')),
                ULE(c(i), ord('9'))
            )
        )
    )

solver.add(c(0) + c(1) + c(2) + c(3) + c(4) + c(5) + c(6) + c(7) + c(8) +
           c(9) + c(10) + c(11) + c(12) + c(13) == targetFullSum)

solver.add(c(0) + c(2) + c(4) + c(6) + c(8) + c(10) + c(12) == targetEvenSum)

solver.add(c(1) + c(3) + c(5) + c(7) + c(9) + c(11) + c(13) == targetOddSum)

print(solver.check())
model = solver.model()

result = ""
for i in range(0, 14):
    result += chr(model[c(i)].as_long())

print(result)
