## Принцип работы алгоритма проверки пароля
1. Вставить символы пароля (14 символов) в ячейки 0-13
2. Посчитать сумму всех символов по модулю 256 в ячейке 43
3. Посчитать сумму символов на четных позициях в ячейке 44
4. Посчитать сумму символов на нечетных позициях в ячейке 45
5. Если все суммы совпадают с нужными значениями, выставить нужное значение в ячейке результата (49)

## Решение reverse-части
Нужные значения сумм - 108, 191, 173. Нужно подобрать пароль, который даст такие суммы. Программа для решения [тут](/reverse/solver.py). Пример подходящего пароля - KzdYuowYBssNoQ.

## Решение pwn-части
Нужные значения сумм - 109, 191, 173. Подходящих паролей с учетом ограничений не существует. Но программа допускает ввод символов, которые изменят поведение программы. Нужно добраться до else-флага финальной проверки на позиции 48, а затем вернуться обратно, чтобы не сломать программу. Для этого до выполнения кода инициализации пароля в памяти устанавливаются несколько отметок: -2 на позиции 14 и -1 на позиции 50.  
Сначала нужно добраться до отметки -1 (по пути увеличивая всё на 1, при этом установив -1 в 0, а -2 в -1): `+[>+]`  
Затем установить else-флаг (48) в 2 (потому что потом он уменьшится на 1), при этом восстановив 0 в ячейке результата (49): `<-<+`  
И наконец нужно вернуться в ячейку 14, где указатель оказывается после инициализации пароля в обычных условиях. Для этого можно использовать -1 на позиции 14. При этом все промежуточные ячейки уменьшатся на 1: `[-<+]`  
Итоговый эксплоит: `+[>+]<-<+[-<+]`.
