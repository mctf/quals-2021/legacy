fun main() {
    val passwordLength = 14
    val password = "KzdYuowYBssNoQ"

    if (password.length != passwordLength) {
        throw IllegalArgumentException()
    }

    val fullSum = 108
    val evenSum = 191
    val oddSum = 173

    val payload = passwordInit(password)
    val program = buildProgram(passwordLength, payload, fullSum, evenSum, oddSum)

    run(program.filter { VALID_CHARS.contains(it) })

}