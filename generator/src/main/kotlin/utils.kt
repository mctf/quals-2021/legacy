fun moveRight(n: Int) = ">".repeat(n)
fun moveLeft(n: Int) = "<".repeat(n)

fun add(n: Int) = "+".repeat(n)
fun subtract(n: Int) = "-".repeat(n)

fun debug() = "~"

fun moveAdding(offset: Int) = """
    [    while A is not null
      -  decrease A
      ${moveRight(offset)}+ increase next cell
      ${moveLeft(offset)}  go back to A
    ]    end loop

""".trimIndent()

fun copy(offset: Int) = """
    [    while A is not null
      -  decrease A
      ${moveRight(offset)}+ increase 1st next cell
      ${moveRight(offset)}+ increase 2nd next cell
      ${moveLeft(offset * 2)}  go back to A
    ]    end loop
    ${moveRight(offset * 2)}
    [
    -
    ${moveLeft(offset * 2)}
    +
    ${moveRight(offset * 2)}
    ]
    ${moveLeft(offset * 2)} go to 2nd copy; move it to source location; then go back to source location
    
""".trimIndent()