fun main() {
    val passwordLength = 14

    // no passwords match those conditions:
    val fullSum = 109
    val evenSum = 191
    val oddSum = 173

    val exploit = buildString {
        // 0 0 -2 0 0 -1
        append("+[>+]") // length = 5
        // 1 1 -1 1 1 0
        append("<-<+")  // length = 4
        // 1 1 -1 1 0 1
        append("[-<+]") // length = 5
        // 1 1 0 0 0 1
    }
    // +[>+]<-<+[-<+]

    if (exploit.length != passwordLength) {
        throw IllegalArgumentException()
    }

    println("Exploit: $exploit")

    val program = buildProgram(passwordLength, exploit, fullSum, evenSum, oddSum)

    run(program.filter { VALID_CHARS.contains(it) })

//    val payload = debug().repeat(50) // replace with user's payload
//    val template = buildProgram(passwordLength, payload, fullSum, evenSum, oddSum)
//    println(template.filter { VALID_CHARS.contains(it) }.replace(payload, "[password]"))
}