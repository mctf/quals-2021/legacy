fun buildProgram(
    passwordLength: Int,
    payload: String,
    fullSum: Int,
    evenSum: Int,
    oddSum: Int
) = buildString {
    /*
     * 0-13  - original password
     * 14    - -2 mark
     * 15-28 - first copy of password (only even indexes)
     * 29-42 - second copy of password (only odd indexes)
     * 43    - sum of all chars (mod 256)
     * 44    - sum of chars on even indexes (mod 256)
     * 45    - sum of chars on odd indexes (mod 256)
     * 46    - else flag
     * 47    - number of correct sums
     * 48    - else flag for the final check
     * 49    - storage for the "+" char
     * 50    - -1 mark
     */

    append(moveRight(passwordLength))
    append("--")
    append(moveRight(passwordLength * 3 + 8 - passwordLength))
    append("-")
    append(moveLeft(passwordLength * 3 + 8))

//        append(debug())

    append(payload)

    append(moveLeft(passwordLength))

    // pointer on 0

    repeat(passwordLength) {
        if (it % 2 == 0) {
            append(copy(passwordLength + 1))
        }
        append('>')
    }

    // pointer at the end of the original password

    append(moveLeft(passwordLength))

    // pointer on 0

    repeat(passwordLength) {
        if (it % 2 == 1) {
            append(copy(passwordLength * 2 + 1))
        }
        append('>')
    }

    append(moveLeft(passwordLength))

//        append(debug())

    // pointer on 0

    repeat(passwordLength) {
        append(moveAdding((passwordLength * 3) + 1 - it))
        append('>')
    }

//        append(debug())

    // pointer at the end of the original password / -2 mark

    append(">")

    // pointer at the end of the start of first copy

    repeat(passwordLength) {
        append(moveAdding((passwordLength * 2) - it + 1))
        append('>')
    }

//        append(debug())

    // pointer at the start of the second copy

    repeat(passwordLength) {
        append(moveAdding(passwordLength - it + 2))
        append('>')
    }

//        append(debug())

    // pointer at the first sum

    append(subtract(fullSum))
    append(">")
    append(subtract(evenSum))
    append(">")
    append(subtract(oddSum))
    append("<<")

    /*
    0 1 0
    ^
    */

    append(">>>+<<<[[-]>>>-<<<]>>>[>+<-]<<<")
    append(">")

//        append(debug())

    /*
    0 1 0
      ^
    */

    append(">>+<<[[-]>>-<<]>>[>+<-]<<")
    append(">")

//        append(debug())

    /*
    0 1 0
        ^
    */

    append(">+<[[-]>-<]>[>+<-]<")
    append(">>")

//        append(debug())

    /*
    0 1 0 n
          ^
    */

    append(subtract(3))
//        append(debug())

    /*
    if (n == 0) print
    */

    append(">+<[[-]>-<]>[>${add('+'.code)}<-]<")
}