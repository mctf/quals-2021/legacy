fun passwordInit(pass: String) =
    pass.replace(Regex("[A-Za-z0-9]")) {
        val letter = it.groupValues[0][0]
        buildString {
            append("+".repeat(letter.code))
            append('>')
        }
    }

fun main() {
    val passwordLength = 14
    val password = "KzdYuowYBssNoQ"

    if (password.length != passwordLength) {
        throw IllegalArgumentException()
    }

    var fullSum = 0
    var evenSum = 0
    var oddSum = 0

    repeat(passwordLength) {
        fullSum += password[it].code
        if (it % 2 == 0) {
            evenSum += password[it].code
        }

        if (it % 2 == 1) {
            oddSum += password[it].code
        }
    }

    fullSum %= 256
    evenSum %= 256
    oddSum %= 256

    println("Full sum: $fullSum")
    println("Even sum: $evenSum")
    println("Odd sum: $oddSum")
    println("-----------")

    val payload = passwordInit(password)
//    val payload = debug().repeat(50) // replace with user's payload
    val program = buildProgram(passwordLength, payload, fullSum, evenSum, oddSum)

//    println(program.filter { VALID_CHARS.contains(it) }.replace(payload, "[password]"))
    run(program.filter { VALID_CHARS.contains(it) })

}